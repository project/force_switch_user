<style type="text/css">
  
</style>
<div id="force-swith-user-widget-wrapper" class="<?php echo variable_get('force_switch_user_skin', 'blue') . ' ' .  variable_get('force_switch_user_position', 'border-middle-left');?>">
  <div class="loading">
    <img src="<?php echo base_path() . drupal_get_path('module', 'force_switch_user') . '/assets/imgs/loading.gif';?>" alt="Loading">
  </div>
  <div class="btn-toogle-show-hide">
    <img src="<?php echo base_path() . drupal_get_path('module', 'force_switch_user') . '/assets/imgs/switch2.png';?>" alt="Swith User">
  </div>
  <div class="content">
    <?php if (!isset($_SESSION['force_switch_user_logged'])): ?>
      <ul class="fsu-login-tab">
        <li id="fsu-user-tab" class="active">FSU User</li>
        <?php 
          if ($uid == 1) {
            echo '<li id="fsu-master-tab">FSU Master</li>';
          }
        ?>
        
      </ul>
      <div class="user-authen-form active">
        <label for="edit-force-switch-user-username">Username</label>
        <input type="text" id="edit-force-switch-user-username" name="force_switch_user_username" placeholder="Enter username...">
        <label for="edit-force-switch-user-password">Password</label>
        <input type="password" id="edit-force-switch-user-password" name="force_switch_user_password" placeholder="Enter password...">

        <div class="form-actions form-wrapper" id="fsu-logion-actions">
          <label class="res-message"></label>
          <input type="submit" id="fsu-logion-user" name="fsu-logion" value="Login" class="form-submit">
        </div>
      </div>
      <?php 
        if ($uid == 1) {
          $form_login = 
            '<div class="master-authen-form">
              <label for="edit-force-switch-master-password">Master password</label>
              <input type="password" id="edit-force-switch-master-password" name="force_switch_master_password" placeholder="Enter master password...">

              <div class="form-actions form-wrapper" id="fsu-logion-actions">
                <label class="res-message"></label>
                <input type="submit" id="fsu-logion-master" name="fsu-logion" value="Login" class="form-submit">
              </div>
            </div>';
          echo $form_login;
        }
      ?>
    <?php else: ?>
      <div class="search-form">
        <?php
          $account = $_SESSION['force_switch_user_logged'];
          echo '<label>Hello ' . $account['username'] . '</label>';
        ?>
        <input type="text" name="user-search" placeholder="Find user...">
        <div class="result-wrapper">

        </div>
      </div>

      <div class="search-recent">
        
      </div>

      <div class="options">
        <span class="btn-logout">Logout</span>
      </div>
    <?php endif ?>
  </div>
</div>