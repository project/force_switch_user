<?php

/**
 * Callback function for get user to switch 
 */
function force_switch_user_ajax_get_users() {
  global $user;
  $results = array();

  $fsu_account = $_SESSION['force_switch_user_logged'];
  $fsu_roles = db_select('fsu_accounts', 'ac')
              ->fields('ac', array('user_role'))
              ->condition('ac.account_id', $fsu_account['account_id'], '=')
              ->execute()
              ->fetchAll();
  $fsu_roles = unserialize($fsu_roles[0]->user_role);

  if (isset($_POST['name'])) {
    $user_name = $_POST['name'];
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'user')
      ->propertyCondition('uid', $user->uid, '!=')
      ->propertyCondition('status', 1)
      ->propertyCondition('name', '%' . $user_name . '%', 'LIKE')
      ->range(0, 10)
      ;
    $result = $query->execute();

    if (isset($result['user'])) {
      $uids = array_keys($result['user']);
      if ($uids) {
        $users = user_load_multiple($uids);
        $users = force_switch_user_role_is_valid($users, $fsu_roles);
        foreach ($users as $key_u => $u) {
          $item = array(
            'value' => $u->uid,
            'label' => $u->name
          );
          $results[] = $item;
        }
      }
    }
  }
  echo json_encode($results);
  exit;
}

/**
 * Callback function for checking login Force Swith User form
 */
function force_switch_user_ajax_user_login() {
  $result = array();

  $client_ip = force_switch_user_get_client_ip();
  if (force_switch_user_check_exist_flood($client_ip) == FALSE
      && variable_get('force_switch_user_enable_flood', 0)) {
    force_switch_user_create_flood($client_ip);
  }
  else if (variable_get('force_switch_user_enable_flood', 0)) {
    $checkFlood = force_switch_user_check_flood($client_ip);
  }

  if(isset($checkFlood) && $checkFlood['status'] == FALSE) {
    $result['status'] = 'fail';
    $result['code'] = 0;
    $result['message'] = $checkFlood['message'];
    echo json_encode($result); exit;
  }

  if (!empty($_POST['type']) && !empty($_POST['type'] == 'master') && !empty($_POST['password'])) {
    $master_pass_hash = variable_get('force_switch_user_master_password', '');
    if (!empty($master_pass_hash) && password_verify($_POST['password'], $master_pass_hash)) {
      $_SESSION['force_switch_user_logged'] = array(
        'account_id' => '1',
        'username' => 'admin',
      );
      if (variable_get('force_switch_user_flush_all_cache', 0)) {
        drupal_flush_all_caches();
      }

      $result['status'] = 'success';
      $result['code'] = 1;
      $result['message'] = 'Admin have logged in successfully';
      $result['template'] = force_switch_user_widget_render();
      if (variable_get('force_switch_user_enable_flood', 0)) {
        force_switch_user_update_flood($client_ip, TRUE);
      }
    }
    else {
      $result['status'] = 'fail';
      $result['code'] = 2;
      $result['message'] = 'Invalid master password';
      if (variable_get('force_switch_user_enable_flood', 0)) {
        force_switch_user_update_flood($client_ip);
      }
      
    }
  }
  else if (!empty($_POST['type']) && !empty($_POST['type'] == 'user')
        &&!empty($_POST['password']) && !empty($_POST['username'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $account = db_select('fsu_accounts', 'ac')
                    ->fields('ac')
                    ->condition('ac.username', $username, '=')
                    ->condition('ac.active', 1 , '=')
                    ->execute()
                    ->fetchAssoc();
    
    if (isset($account) && password_verify($password, $account['password'])) {
      $_SESSION['force_switch_user_logged'] = array(
        'account_id' => $account['account_id'],
        'username' => $username,
      );
      if (variable_get('force_switch_user_flush_all_cache', 0)) {
        drupal_flush_all_caches();
      }

      $result['status'] = 'success';
      $result['code'] = 1;
      $result['message'] = $username . ' have logged in successfully';
      $result['template'] = force_switch_user_widget_render();
      if (variable_get('force_switch_user_enable_flood', 0)) {
        force_switch_user_update_flood($client_ip, TRUE);
      }
    } else {
      $result['status'] = 'fail';
      $result['code'] = 2;
      $result['message'] = 'Invalid password or username';
      if (variable_get('force_switch_user_enable_flood', 0)) {
        force_switch_user_update_flood($client_ip);
      }
    }
  } 
  else {
    $result['status'] = 'fail';
    $result['code'] = 0;
    $result['message'] = 'Bad request';
    if (variable_get('force_switch_user_enable_flood', 0)) {
      force_switch_user_update_flood($client_ip);
    }
  }
  echo json_encode($result); exit;
}

/**
 * Callback function for logout Force Switch User form
 */
function force_switch_user_ajax_user_logout() {
  unset($_SESSION['force_switch_user_logged']);
  echo force_switch_user_widget_render(); exit;
}

/**
 * Callback function for switch User
 */
function force_switch_user_ajax_user_switch() {
  $result = 0;
  if (force_switch_user_is_logged()) {
    if (!empty($_POST['uid'])) {
      $uid = $_POST['uid'];
      $u = user_load($uid);
      if (isset($u)) {
        $form_state = array();
        $form_state['uid'] = $u->uid;      
        user_login_submit(array(), $form_state);
        $result = 1;
      }
    }
  }
  echo $result; exit;
}

/**
 * Function create flood to resist network attack
 */
function force_switch_user_create_flood($client_ip) {
  $fid = db_insert('fsu_flood')
                ->fields(array(
                  'identifier' => $client_ip,
                  'login_count' => 0,
                  'created' => REQUEST_TIME,
                ))
                ->execute();
}

/**
 * Function check exist flood to validate form
 */
function force_switch_user_check_exist_flood($client_ip) {
  $info_flood = db_select('fsu_flood', 'fl')
                  ->fields('fl')
                  ->condition('identifier', $client_ip, '=')
                  ->execute()
                  ->fetchAll();
  //check exist of ip
  if(!empty($info_flood)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Function check flood to validate form
 */
function force_switch_user_check_flood($client_ip) {
  $info_flood = db_select('fsu_flood', 'fl')
                  ->fields('fl')
                  ->condition('identifier', $client_ip, '=')
                  ->execute()
                  ->fetchObject();
  $time_now = time();
  $expiration = $info_flood->expiration;
  //check expiration of ip
  if($info_flood->expiration != 0) {
    if ($expiration > $time_now) {
      $diff_time = ($expiration - $time_now)/60;
      $message = "Login fail more 5 times,please try again after " . round($diff_time) . ' minutes!!!';
      return array('status' => FALSE, 'message' => $message);
    }
    else{
      //reset flood for client ip 
      $info_flood = db_update('fsu_flood')
                  ->fields(array(
                    'login_count' => 0,
                    'expiration' => 0,
                    'changed' => REQUEST_TIME
                    ))
                  ->condition('identifier', $client_ip, '=')
                  ->execute();
    }
  }
  return array('status' => TRUE, 'message' => '');
}

/**
 * Function update flood to validate form
 */
function force_switch_user_update_flood($client_ip, $login_success = FALSE) {
  $info_flood = db_select('fsu_flood', 'fl')
                  ->fields('fl')
                  ->condition('identifier', $client_ip, '=')
                  ->execute()
                  ->fetchObject();
  
  if (isset($info_flood)) {
    if ($login_success == FALSE) {
      $login_count = $info_flood->login_count + 1;
      if ($login_count == 5) {
        $expiration = strtotime("+30 minutes");
      }
      else {
        $expiration = $info_flood->expiration;
      }
      $info_flood = db_update('fsu_flood')
                    ->fields(array(
                      'login_count' => $login_count,
                      'expiration' => $expiration,
                      'changed' => REQUEST_TIME
                      ))
                    ->condition('identifier', $client_ip, '=')
                    ->execute();
    }
    else {
      $info_flood = db_update('fsu_flood')
                    ->fields(array(
                      'login_count' => 0,
                      'expiration' => 0,
                      'changed' => REQUEST_TIME
                      ))
                    ->condition('identifier', $client_ip, '=')
                    ->execute();
    }
  }
}