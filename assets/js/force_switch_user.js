(function ($) {
  Drupal.FSUser = Drupal.FSUser || {};
  Drupal.FSUser.widget = null;
  Drupal.FSUser.searchForm = null;
  Drupal.FSUser.searchRecent = null;
  Drupal.FSUser.authenForm = null;
  Drupal.FSUser.loading = null;

  Drupal.behaviors.FSUser = {
    attach: function (context, settings) {
      Drupal.FSUser.init();
    }
  };

  Drupal.FSUser.init = function () {
    Drupal.FSUser.widget = $('div#force-swith-user-widget-wrapper');
    Drupal.FSUser.searchForm = Drupal.FSUser.widget.find('div.search-form');
    Drupal.FSUser.searchRecent = Drupal.FSUser.widget.find('div.search-recent');
    Drupal.FSUser.authenUserForm = Drupal.FSUser.widget.find('div.user-authen-form');
    Drupal.FSUser.authenMasterForm = Drupal.FSUser.widget.find('div.master-authen-form');
    Drupal.FSUser.loading = Drupal.FSUser.widget.find('div.loading');
    Drupal.FSUser.showHide();
    Drupal.FSUser.searchActions();
    Drupal.FSUser.recentUser();
    Drupal.FSUser.endActions();
    Drupal.FSUser.malihuScroll();
    Drupal.FSUser.eventEnter();
  };

  Drupal.FSUser.endActions = function () {
    Drupal.FSUser.widget.appendTo("body");
    Drupal.FSUser.loading.hide();
  };

  Drupal.FSUser.showHide = function () {
    Drupal.FSUser.widget.find('div.btn-toogle-show-hide').click(function () {
      Drupal.FSUser.widget.toggleClass('show');
    });
    Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-master-tab').click(function () {
      if (Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-user-tab').hasClass('active')) {
        Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-user-tab').removeClass("active");
      }
      if (Drupal.FSUser.widget.find('div.user-authen-form').hasClass('active')) {
        Drupal.FSUser.widget.find('div.user-authen-form').removeClass("active");
      }
      $(this).addClass('active');
      Drupal.FSUser.widget.find('div.master-authen-form').addClass('active');
    });
    Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-user-tab').click(function () {
      if (Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-master-tab').hasClass('active')) {
        Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-master-tab').removeClass("active");
      }
      if (Drupal.FSUser.widget.find('div.master-authen-form').hasClass('active')) {
        Drupal.FSUser.widget.find('div.master-authen-form').removeClass("active");
      }
      $(this).addClass('active');
      Drupal.FSUser.widget.find('div.user-authen-form').addClass('active');
    });
    if (Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-master-tab').length == 0) {
      Drupal.FSUser.widget.find('ul.fsu-login-tab li#fsu-user-tab').addClass("fsu-format-user");
    }
  };

  Drupal.FSUser.searchActions = function () {
    Drupal.FSUser.searchForm.find('input[name="user-search"]').keyup(function () {
      var name = Drupal.FSUser.searchForm.find('input[name="user-search"]').val();
      if (name != '') {
        $.ajax({
          url: Drupal.settings.basePath + 'ajax/force-swith-user/users',
          type: "POST",
          data: {
            name: name
          },
          success: function (response) {
            var data = JSON.parse(response);
            // class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front"
            var markup = '<ul>';
            $.each(data, function (k, e) {
              markup += '<li class="select-user" data-uid="' + e.value + '" data-name="' + e.label + '">';
              markup += e.label;
              markup += '</li>';
            });
            markup += '</ul>';
            Drupal.FSUser.searchForm.find('div.result-wrapper').html(markup);
            Drupal.FSUser.widget.find('.result-wrapper ul').mCustomScrollbar({
              scrollAmount: 20
            });
            Drupal.FSUser.switchUserActions();
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      } else {
        Drupal.FSUser.searchForm.find('div.result-wrapper').html('');
      }
    });

    //Clear data search
    Drupal.FSUser.widget.click(function () {
      var checkFocus = Drupal.FSUser.searchForm.find('input[name="user-search"]').is(':focus');
      if (checkFocus == false) {
        Drupal.FSUser.searchForm.find('div.result-wrapper').html('');
      }
    });

    // Drupal.FSUser.searchForm.find('input[name="user-search"]').autocomplete({
    //   source: Drupal.settings.basePath + 'ajax/force-swith-user/users',
    //   minLength: 1,
    //   select: function( event, ui ) {
    //     var uid = ui['item'].value;
    //     var name = ui['item'].label;
    //     Drupal.FSUser.switchUser(uid, name);
    //   }
    // });

    Drupal.FSUser.switchUser = function (uid, name) {
      Drupal.FSUser.loading.show();
      $.ajax({
        url: Drupal.settings.basePath + 'ajax/force-swith-user/switch-user',
        type: "POST",
        data: {
          uid: uid
        },
        success: function (response) {
          Drupal.FSUser.searchForm.find('input[name="user-search"]').val(name);
          if (response == 1) {
            // Save user to local storage
            var fsuList = Array();
            var u = {
              uid: uid,
              name: name,
              time: Date.now()
            }

            if (localStorage.fsuHistory) {
              fsuList = JSON.parse(localStorage.fsuHistory);
            }

            $.each(fsuList, function (k, e) {
              if (typeof e != 'undefined' && typeof u != 'undefined' && e != null) {
                if (e.uid == u.uid) {
                  fsuList.splice(k, 1);
                }
              }
            });
            fsuList.push(u);

            localStorage.fsuHistory = JSON.stringify(fsuList);
            Drupal.FSUser.loading.hide();
            var url = window.location.origin + window.location.pathname;
            window.location.replace(url);
          };
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    };

    Drupal.FSUser.switchUserActions = function () {
      Drupal.FSUser.searchForm.find('div.result-wrapper ul li.select-user').unbind('click');
      Drupal.FSUser.searchForm.find('div.result-wrapper ul li.select-user').click(function (e) {
        var uid = $(this).attr('data-uid');
        var name = $(this).attr('data-name');
        Drupal.FSUser.switchUser(uid, name);
      });
    };

    Drupal.FSUser.authenUserForm.find('input#fsu-logion-user').click(function () {
      var username = Drupal.FSUser.authenUserForm.find('input[name="force_switch_user_username"]').val();
      var password = Drupal.FSUser.authenUserForm.find('input[name="force_switch_user_password"]').val();
      Drupal.FSUser.loading.show();
      $.ajax({
        url: Drupal.settings.basePath + 'ajax/force-swith-user/login',
        type: "POST",
        data: {
          'type': 'user',
          'username': username,
          'password': password
        },
        success: function (response) {
          setTimeout(function () {
            var data = JSON.parse(response);
            if (data.status == 'success' && data.code == 1) {
              // Rebuild update widget
              Drupal.FSUser.widget.replaceWith(data.template);
              Drupal.FSUser.init();
              Drupal.FSUser.widget.addClass('show');
            } else {
              Drupal.FSUser.authenUserForm.find('label.res-message').html(data.message);
              Drupal.FSUser.authenUserForm.find('label.res-message').show();
              setTimeout(function () {
                Drupal.FSUser.authenUserForm.find('label.res-message').hide();
              }, 3000);
            }
            Drupal.FSUser.loading.hide();
          }, 1000);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
          Drupal.FSUser.loading.hide();
        }
      });
    });
    Drupal.FSUser.authenMasterForm.find('input#fsu-logion-master').click(function () {
      var $password = Drupal.FSUser.authenMasterForm.find('input[name="force_switch_master_password"]').val();
      Drupal.FSUser.loading.show();
      $.ajax({
        url: Drupal.settings.basePath + 'ajax/force-swith-user/login',
        type: "POST",
        data: {
          'type': 'master',
          'password': $password
        },
        success: function (response) {
          setTimeout(function () {
            var data = JSON.parse(response);
            if (data.status == 'success' && data.code == 1) {
              // Rebuild update widget
              Drupal.FSUser.widget.replaceWith(data.template);
              Drupal.FSUser.init();
              Drupal.FSUser.widget.addClass('show');
            } else {
              Drupal.FSUser.authenMasterForm.find('label.res-message').html(data.message);
              Drupal.FSUser.authenMasterForm.find('label.res-message').show();
              setTimeout(function () {
                Drupal.FSUser.authenMasterForm.find('label.res-message').hide();
              }, 3000);
            }
            Drupal.FSUser.loading.hide();
          }, 1000);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
          Drupal.FSUser.loading.hide();
        }
      });
    });

    Drupal.FSUser.widget.find('span.btn-logout').click(function () {
      Drupal.FSUser.loading.show();
      $.ajax({
        url: Drupal.settings.basePath + 'ajax/force-swith-user/logout',
        type: "POST",
        data: {},
        success: function (response) {
          setTimeout(function () {
            Drupal.FSUser.widget.replaceWith(response);
            Drupal.FSUser.loading.hide();
            Drupal.FSUser.init();
            Drupal.FSUser.widget.addClass('show');
          }, 1000);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
          Drupal.FSUser.loading.hide();
        }
      });
    });
  };

  Drupal.FSUser.recentUser = function () {
    if (localStorage.fsuHistory) {
      fsuList = JSON.parse(localStorage.fsuHistory);
      fsuList.reverse();
      var markup = '<label>Recent user log</label>';
      markup += '<ul class="recent-list">';
      $.each(fsuList, function (k, e) {
        markup += '<li>';
        markup += '<div class="left">';
        markup += e.name;
        markup += '<span class="time-logged"> <i>(' + Drupal.FSUser.timeSince(e.time) + ' ago)</i></span>';
        markup += '</div>';
        markup += '<span data-name="' + e.name + '" data-uid="' + e.uid + '" class="switch-user btn">Switch</span>';
        markup += '</li>';
      });
      markup += '</ul>';
      Drupal.FSUser.searchRecent.html(markup);
    }

    Drupal.FSUser.searchRecent.find('ul.recent-list li span.switch-user').click(function () {
      var uid = $(this).attr('data-uid');
      var name = $(this).attr('data-name');
      Drupal.FSUser.switchUser(uid, name);
    });
  };

  Drupal.FSUser.timeSince = function (date) {
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 31536000);
    if (interval > 1) {
      return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
  }

  Drupal.FSUser.malihuScroll = function () {
    Drupal.FSUser.widget.find('ul.recent-list').mCustomScrollbar({
      scrollAmount: 20
    });

  }

  Drupal.FSUser.eventEnter = function () {
    Drupal.FSUser.authenUserForm.keypress(function (event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13') {
        Drupal.FSUser.authenUserForm.find('input#fsu-logion-user').trigger('click');
      }
    });
    Drupal.FSUser.authenMasterForm.keypress(function (event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13') {
        Drupal.FSUser.authenMasterForm.find('input#fsu-logion-master').trigger('click');
      }
    });
  }




})(jQuery);