<?php

/**
 * Form for Config Force Switch User
 */
function force_switch_user_config_form() {
  $form = array();
  $form['configs'] = array(
    '#type' => 'fieldset',
    '#title' => 'Configurations'
  );

  $form['configs']['force_switch_user_enable'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable',
    '#default_value' => variable_get('force_switch_user_enable', 0),
    '#weight' => 10
  );

  $form['configs']['force_switch_user_flush_all_cache'] = array(
    '#type' => 'checkbox',
    '#title' => 'Flush all caches when switching to new user',
    '#default_value' => variable_get('force_switch_user_flush_all_cache', 0),
    '#weight' => 20
  );

  $form['configs']['force_switch_user_enable_flood'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable brute force protection',
    '#default_value' => variable_get('force_switch_user_enable_flood', 0),
    '#weight' => 30
  );

  $form['configs']['force_switch_user_master_password_text'] = array(
    '#type' => 'password',
    '#title' => t('Master password'),
    '#description' => t('Master password for switching users'),
    '#weight' => 40
  );

  $form['configs']['force_switch_user_skin'] = array(
    '#type' => 'select',
    '#title' => 'Skin',
    '#options' => array(
      'blue' => t('Blue'),
      'red' => t('Red'),
      'green' => t('Green'),
      'yellow' => t('Yellow'),
      'gray' => t('Gray'),
      'orange' => t('Orange'),
      'black' => t('Black'),
      'pink' => t('Pink'),
      'violet' => t('Violet')
    ),
    '#default_value' => variable_get('force_switch_user_skin', 'blue'),
    '#weight' => 50
  );

  $form['configs']['force_switch_user_position'] = array(
    '#type' => 'select',
    '#title' => 'Position',
    '#options' => array(
      'position-top-left' => 'Left Top',
      'position-middle-left' => 'Left Middle',
      'position-bottom-left' => 'Left Bottom',
      'position-top-right' => 'Right Top',
      'position-middle-right' => 'Right Middle',
      'position-bottom-right' => 'Right Bottom',
      'position-bottom' => 'Bottom'
    ),
    '#default_value' => variable_get('force_switch_user_position', 'Left Middle'),
    '#weight' => 60
  );

  $form['configs']['user_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles'),
    '#description' => t('User roles that can use this function'),
    '#required' => FALSE,
    '#options' => user_roles(),
    '#size' => 5,
    '#weight' => 0,
    '#default_value' => variable_get('user_roles', array()),
    '#weight' => 70
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'force_switch_user_config_form_submit';
  return $form;
}

/**
 * Set config Force Switch User
 */
function force_switch_user_config_form_submit($form, &$form_state) {
  $input = $form_state['input'];
  if (isset($input['force_switch_user_master_password_text']) && !empty($input['force_switch_user_master_password_text'])) {
    $hash = password_hash($input['force_switch_user_master_password_text'], PASSWORD_DEFAULT);
    variable_set('force_switch_user_master_password', $hash);
  }
}