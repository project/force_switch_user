<?php
module_load_include('inc', 'force_switch_user', 'forms/force_switch_user.log.form');

/**
 * Create form for creating account.
 */
function force_switch_user_create_account_form($form, &$form_state) {
  if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];
    $account = db_select('fsu_accounts', 'ac')
                  ->fields('ac')
                  ->condition('account_id', $id, '=')
                  ->execute()
                  ->fetchAll();
    $form_state['account'] = $account;
    $user_roles = unserialize($account[0]->user_role);
  }
  $form = array();

  $form['fsu_username'] = array(
    '#type' => 'textfield',
    '#title' => t('FSU Username'),
    '#default_value' => isset($account) ? t($account[0]->username) : '',
    '#description' => t("The username of account."),
    '#required' => TRUE,
    '#disabled' => isset($account) ? TRUE : FALSE,
    '#weight' => 10
  );

  $form['fsu_password'] = array(
    '#type' => 'password_confirm',
    '#title' => t('Confirm Password'),
    '#title_display' => 'invisible',
    '#description' => t("The password of account."),
    '#required' => isset($account) ? FALSE : TRUE,
    '#weight' => 20
  );

  $form['fsu_active'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#default_value' => isset($account) ? $account[0]->active : 1,
    '#options' => array(
      1 => t('Active'),
      0 => t('Inactive')
    ),
    '#weight' => 30
  );

  $form['fsu_user_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('FSU User roles'),
    '#description' => t('User roles that can use for filter Drupal User'),
    '#required' => FALSE,
    '#options' => user_roles(TRUE),
    '#size' => 5,
    '#default_value' => isset($account) ? $user_roles : array(),
    '#weight' => 40
  );

  $form['fsu_btn_create'] = array(
    '#type' => 'submit',
    '#value' => isset($account) ? t('Update') : t('Create'),
    '#description' => t("Create account."),
    '#weight' => 50
  );
  return $form;
}

/**
 * The create and update accounts form.
 */
function force_switch_user_create_account_form_submit(&$form, &$form_state) {
  global $base_url;
  $message_log = '';
  $username = $form_state['values']['fsu_username'];
  $user_roles = $form_state['values']['fsu_user_roles'];
  $user_roles = serialize($user_roles);
  $active = $form_state['values']['fsu_active'];
  if (!empty($form_state['values']['fsu_password'])) {
    $pass_hash = password_hash($form_state['values']['fsu_password'], PASSWORD_DEFAULT);
  }
  else {
    $pass_hash = $form_state['account'][0]->password;
  }
  $check_user = db_select('fsu_accounts', 'ac')
                  ->fields('ac')
                  ->condition('ac.username', $username, '=')
                  ->execute()
                  ->fetchAssoc();

  if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];
    $account_id = db_update('fsu_accounts')
            ->fields(array(
              // 'username' => $username,
              'password' => $pass_hash,
              'user_role' => $user_roles,
              'active' => $active,
              'changed' => REQUEST_TIME,
            ))
            ->condition('account_id', $id, '=')
            ->execute();

    $message_log = $username . ' is updated!!';
    force_switch_user_create_log($account_id, $message_log);
    drupal_set_message($username . ' is updated!!');
    $form_state['redirect'] = $base_url . '/admin/config/user-interface/force-swith-user-settings/accounts';
  }
  else {
    if ($check_user) {    
      $path = current_path();
      drupal_set_message($username . ' existed!!','error');
      $form_state['redirect'] = $path;
    }
    else {
      $account_id = db_insert('fsu_accounts')
                ->fields(array(
                  'username' => $username,
                  'password' => $pass_hash,
                  'user_role' => $user_roles,
                  'active' => $active,
                  'created' => REQUEST_TIME,
                ))
                ->execute();
      
      $message_log = $username . ' is created!!';
      force_switch_user_create_log($account_id, $message_log);
      drupal_set_message($username . ' is created!!');
      $form_state['redirect'] = $base_url . '/admin/config/user-interface/force-swith-user-settings/accounts';
    }
  }
}

/**
 * Deleting account form.
 */
function force_switch_user_delete_account_form ($form, &$form_state) {
  if (isset($_GET['id']) && !empty($_GET['id'])) {
    $path = 'admin/config/user-interface/force-swith-user-settings/accounts';
    $username = $_GET['username'];
    $message = t('Are you sure you want to delete the account %name?', array('%name' => $username));
    $caption = '<p>' . t('This action cannot be undone.') . '</p>';
    return confirm_form($form, $message, $path, $caption, t('Delete'));
  }
}

/**
 * Deleting account form.
 */
function force_switch_user_delete_account_form_submit($form, &$form_state) {
  if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];
    $username = $_GET['username'];
    $account_id = db_delete('fsu_accounts')
                  ->condition('account_id', $id, '=')
                  ->execute();

    $message_log = $username . ' is deleted!!';
    force_switch_user_create_log($account_id, $message_log);
    drupal_set_message($username . ' is deleted!!');
    $form_state['redirect'] = 'admin/config/user-interface/force-swith-user-settings/accounts';
  }
}