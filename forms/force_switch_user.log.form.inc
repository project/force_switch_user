<?php

/**
 * Form for showing log of Force Switch User
 */
function force_switch_user_log_form($form, &$form_state) {
  $form = array();
  $result = db_select('fsu_logs', 'l')
              ->fields('l')
              ->extend('PagerDefault')
              ->execute()
              ->fetchAll();
  $row_array = array();
  krsort($result);
  foreach ($result as $value) {
    $date = date('d/m/Y , H:m:s', $value->created);
    $fsu_username = db_select('fsu_accounts','ac')
                      ->fields('ac',array('username'))
                      ->condition('account_id', $value->account_id, '=')
                      ->execute()
                      ->fetchField();
    $data = array($fsu_username, $value->body, $date);
    $row_array[] = array('data' => $data);
  }
  $headers = array(
    'FSU Username',
    'Message',
    'Date',
  );

  $form['fsu_clear_logs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear FSU Log Messages'),
    '#description' => 'This will permanently remove the FSU log messages from the database.',
    '#collapsible' => TRUE, 
    '#collapsed' => TRUE,
  );

  $form['fsu_clear_logs']['fsu_btn_clear_logs'] = array(
    '#type' => 'submit',
    '#value' => 'Clear FSU log messages',
    '#submit' => array('force_switch_user_clear_logs_form'),
  );

  $form['fsu_log_table'] = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $row_array,
    '#empty' => t('Table has no row!')
  );

  $form['pager'] = array('#markup' => theme('pager'));
  
  return $form;
}

/**
 * Writing Log in Database
 */
function force_switch_user_create_log ($acount_id = 1, $message = '') {
  $log_id = db_insert('fsu_logs')
              ->fields(array(
                'account_id' => $acount_id,
                'body' => $message,
                'created' => REQUEST_TIME,
              ))
              ->execute();
}

/**
 * Function clear log Force Switch User
 */
function force_switch_user_clear_logs_form($form, &$form_state) {
  global $base_url;
  $log_id = db_delete('fsu_logs')
              ->execute();
  drupal_set_message('Database fsu log cleared.');
}