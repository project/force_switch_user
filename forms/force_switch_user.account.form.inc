<?php

/**
 * Form for showing list of Force Switch User accounts
 */
function force_switch_user_account_form() {
  global $base_url;
  $form = array();

  $result = db_select('fsu_accounts', 'ac')
              ->fields('ac')
              ->extend('PagerDefault')
              ->execute()
              ->fetchAll();
  $row_array = array();

  foreach ($result as $value) {
    $active = ($value->active == 1) ? 'Yes' : 'No';
    if ($value->account_id != 1) {
      $edit_link = l(t('Edit'), 'admin/config/user-interface/force-swith-user-settings/edit-account', array('query' => array('id' => $value->account_id)));
      $delete_link = l(t('Delete'), 'admin/config/user-interface/force-swith-user-settings/delete-account', array('query' => array('id' => $value->account_id)));

      $operations_markup = implode(' | ', array($edit_link, $delete_link));
      $data = array($value->username, $active, $operations_markup);
      $row_array[] = array('data' => $data);
    }
  }
  $header = array(
    'FSU Username',
    'Active',
    '',
  );

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $row_array,
    '#empty' => t('Table has no row!')
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}